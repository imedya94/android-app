package com.example.myapplication;

public class ListItem {
    private String title;
    private String imageUrl;
    private String address;

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getAddress() {
        return address;
    }
}
