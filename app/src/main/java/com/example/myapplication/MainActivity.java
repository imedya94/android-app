package com.example.myapplication;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;
import android.widget.ListView;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private TextView email,name,text;
    private CallbackManager callbackManager;

    private ListView listItem;// listview to show json data

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name.findViewById(R.id.display_name);
        email.findViewById(R.id.email);
        //imageView.findViewById(R.id.image_view);
        loginButton.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        //creating callbackmanager
        callbackManager = CallbackManager.Factory.create();

        //Registering callbackmanager
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
           @Override
          public void onSuccess(LoginResult loginResult) {
               //retrieving access token by login result
               //AccessToken newAccessToken = loginResult.getAccessToken();
               //loadUser(newAccessToken);

            }

           @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        listItem = findViewById(R.id.list_item);

        //interface implementation
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl("https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        applicationJasonAPI api=retrofit.create(applicationJasonAPI.class);

        Call<List<ListItem>> call = api.getItems();

        call.enqueue(new Callback<List<ListItem>>() {
            @Override
            public void onResponse(Call<List<ListItem>> call, Response<List<ListItem>> response) {
                if(!response.isSuccessful()){
                    text.setText("code:"+ response.code());
                    return;
                }
                List<ListItem> items= response.body();
            }

            @Override
            public void onFailure(Call<List<ListItem>> call, Throwable t) {
                text.setText(t.getMessage());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    AccessTokenTracker accessTokenTracker=new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if(currentAccessToken==null){
                name.setText("");
                email.setText("");
                Toast.makeText(MainActivity.this, "User logged out", Toast.LENGTH_LONG).show();
            }else
                loadUser(currentAccessToken);

        }
    };

    private void loadUser(AccessToken newAccessToken){
        /**
         Creating the GraphRequest to fetch user details
         1st Param - AccessToken
         2nd Param - Callback (which will be invoked once the request is successful)
         **/
        GraphRequest request= GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String first_name=object.getString("first_name");
                    String emailId=object.getString("emailId");
                    //String id=object.getString("id");

                    name.setText(first_name);
                    email.setText(emailId);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        Bundle parameters= new Bundle();
        parameters.putString("field","first_name,emailId");
        request.setParameters(parameters);
        request.executeAsync();
    }

}
