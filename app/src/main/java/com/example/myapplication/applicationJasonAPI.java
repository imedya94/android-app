package com.example.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface applicationJasonAPI {

    @Headers("Content-Type:application/json")
    @GET("hotels.json")
    Call<List<ListItem>> getItems();
}
